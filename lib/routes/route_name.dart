abstract class RouteName {
  static const landingPage = '/page-1';
  static const homePage = '/page-2';
  static const loginPage = '/page-3';
  static const registerPage = '/page-4';
  static const listNews = '/page-5';
}