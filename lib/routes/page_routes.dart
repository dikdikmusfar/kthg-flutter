import 'package:kthg/pages/GetDataScreen.dart';
import 'package:kthg/pages/LoginScreen.dart';
import 'package:kthg/pages/Menu/HomeScreen.dart';
import 'package:kthg/pages/Menu/MainScreen.dart';
import 'package:kthg/pages/RegisterScreen.dart';
import 'package:kthg/test.dart';
import 'package:kthg/routes/route_name.dart';
import 'package:get/get.dart';

class pageRouteApp {
  static final pages = [
    GetPage(
      name: RouteName.landingPage,
      page: () => TestScreen(),
      ),
    GetPage(
      name: RouteName.homePage,
      page: () => MainScreen(),
      ),
    GetPage(
      name: RouteName.loginPage,
      page: () => Login(),
      ),
    GetPage(
      name: RouteName.registerPage,
      page: () => Register(),
      ),
    GetPage(
      name: RouteName.listNews,
      page: () => GetDataScreenStateManagement(),
      ),
  ];
}