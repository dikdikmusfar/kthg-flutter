import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:kthg/pages/Menu/MainScreen.dart';
import 'package:kthg/routes/route_name.dart';
import 'package:get/get.dart';




class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: Text("Login Screen App"),
      // ),
      body: Padding(
        padding: EdgeInsets.all(10),
        child: ListView(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Image.asset(
                "assets/img/logo.png",
                height: 100,
                width: 100,
              ),
            ),
            Container(
              padding: EdgeInsets.all(10),
              margin: const EdgeInsets.only(top: 0),
              child: Text(
                "Hello there!",
                style: TextStyle(
                    color: Colors.red[400],
                    fontWeight: FontWeight.w400,
                    fontSize: 36),
              ),
            ),
            Container(
              padding: EdgeInsets.all(10),
              margin: const EdgeInsets.only(top: 20),
              child: TextField(
                controller: _emailController,
                decoration: InputDecoration(
                    border: OutlineInputBorder(), labelText: "Email "),
              ),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
              margin: const EdgeInsets.only(top: 20),
              child: TextField(
                obscureText: true,
                controller: _passwordController,
                decoration: InputDecoration(
                    border: OutlineInputBorder(), labelText: "Password"),
              ),
            ),
            // FlatButton(
            //   onPressed:(){

            //   },
            //   textColor: Colors.blue,
            //   child: Text("Forgot Password"),
            // ),
            
            SizedBox(height: 20,),

            Container(
              padding: EdgeInsets.all(10),
                child: Row(children: <Widget>[
              TextButton(onPressed: () {}, child: Text("Forgot Password")),
              Text("                                "),
              ElevatedButton(
                // textColor: Colors.white,
                style: raisedButtonStyle,
                child: Text("Sign In"),
                onPressed: () async {
                  await _firebaseAuth.signInWithEmailAndPassword(
                    email: _emailController.text, password: _passwordController.text
                    ).then((value) => Navigator.of(context).pushReplacement(
                      MaterialPageRoute(builder: (context) => MainScreen())
                    ));
                },
              ),
            ])),

            SizedBox(height: 10,),
            Container(
              alignment: Alignment.center,
              child: Text("sign in with", style: TextStyle(color: Colors.blueAccent)),
            ),
            SizedBox(height: 20,),
            Container(
              padding: EdgeInsets.all(10),
              alignment: Alignment.center,
              child: Row(children: <Widget>[
                Text("                    "),
                Image.asset("assets/img/Google.png"),
                Text("          "),
                Image.asset("assets/img/Apple.png"),
                Text("          "),
                Image.asset("assets/img/Facebook.png"),
              ],),
            ),
            SizedBox(height: 10,),
            Container(
              alignment: Alignment.center,
              child: Text("or", style: TextStyle(color: Colors.blueAccent)),
            ),
            SizedBox(height: 10),
            ElevatedButton(
                // textColor: Colors.white,
                style: newraisedButtonStyle,
                child: Text("Sign Up"),
                onPressed: () async {
                  Get.toNamed(RouteName.registerPage);
                },
              ),
          ],
        ),
      ),
    );
  }
}

final ButtonStyle raisedButtonStyle = ElevatedButton.styleFrom(
  onPrimary: Colors.yellow,
  primary: Colors.red[400],
  minimumSize: Size(88, 36),
  padding: EdgeInsets.symmetric(horizontal: 16),
  shape: const RoundedRectangleBorder(
    borderRadius: BorderRadius.all(Radius.circular(6)),
  ),
);

final ButtonStyle newraisedButtonStyle = ElevatedButton.styleFrom(
  onPrimary: Colors.white,
  primary: Colors.blueAccent[400],
  minimumSize: Size(88, 36),
  padding: EdgeInsets.symmetric(horizontal: 16),
  shape: const RoundedRectangleBorder(
    borderRadius: BorderRadius.all(Radius.circular(6)),
  ),
);