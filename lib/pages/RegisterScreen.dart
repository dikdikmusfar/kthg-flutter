import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:kthg/pages/LoginScreen.dart';
import 'package:kthg/routes/route_name.dart';
import 'package:get/get.dart';




class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: Text("Register Screen App"),
      // ),
      body: Padding(
        padding: EdgeInsets.all(10),
        child: ListView(
          children: <Widget>[            
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Image.asset(
                "assets/img/logo.png",
                height: 100,
                width: 100,
              ),
            ),
            Container(
              padding: EdgeInsets.all(10),
              margin: const EdgeInsets.only(top: 0),
              child: Text(
                "Welcome",
                style: TextStyle(
                    color: Colors.red[400],
                    fontWeight: FontWeight.w500,
                    fontSize: 30),
              ),
            ),
            Container(
              padding: EdgeInsets.all(10),
              margin: const EdgeInsets.only(top: 40),
              child: TextField(
                controller: _emailController,
                decoration: InputDecoration(
                    border: OutlineInputBorder(), labelText: "Email "),
              ),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
              child: TextField(
                obscureText: true,
                controller: _passwordController,
                decoration: InputDecoration(
                    border: OutlineInputBorder(), labelText: "Password"),
              ),
            ),
            Container(
              height: 50,
              padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
              margin: const EdgeInsets.only(top: 40),
              child: ElevatedButton(
                // textColor: Colors.white,
                style: raisedButtonStyle,
                child: Text("Sign Up"),
                onPressed: () async {
                  await _firebaseAuth.createUserWithEmailAndPassword(
                    email: _emailController.text, password: _passwordController.text
                    ).then((value) => Navigator.of(context).pushReplacement(
                      MaterialPageRoute(builder: (context) => Login())
                    ));
                },
              ),
            ),
            SizedBox(height: 40,),
            TextButton(onPressed: () async {
              Get.toNamed(RouteName.loginPage);
            }, child: Text("Already have an account?")),
          ],
        ),
      ),
    );
  }
}

final ButtonStyle raisedButtonStyle = ElevatedButton.styleFrom(
  onPrimary: Colors.yellow,
  primary: Colors.red[400],
  minimumSize: Size(88, 36),
  padding: EdgeInsets.symmetric(horizontal: 16),
  shape: const RoundedRectangleBorder(
    borderRadius: BorderRadius.all(Radius.circular(6)),
  ),
);
