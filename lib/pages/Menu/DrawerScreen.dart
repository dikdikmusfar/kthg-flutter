import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:kthg/pages/LoginScreen.dart';
import 'package:kthg/routes/route_name.dart';
import 'package:get/get.dart';


class DrawerScreen extends StatefulWidget {
  @override
  _DrawwerScreenState createState() => _DrawwerScreenState();
}

class _DrawwerScreenState extends State<DrawerScreen>{

  Future<void> _signOut() async {
    await FirebaseAuth.instance.signOut();
  }

  @override
  Widget build(BuildContext context){
    FirebaseAuth auth = FirebaseAuth.instance;
    if(auth.currentUser != null){
      print(auth.currentUser!.email);
    }
    return Drawer(
      child: ListView(
        children: <Widget>[
          UserAccountsDrawerHeader(
            accountName: Text("Dikdik Musfar Pribadi"),
            currentAccountPicture: CircleAvatar(
              backgroundImage: AssetImage("assets/img/dikdik.jpeg")
            ), 
            accountEmail: Text("dikdikmusfarr@gmail.com"),
            ),
          DrawerListTile(
            iconData : Icons.lock,
            title: "Rumpi.. No Secret",
            onTilePressed: () async {
              Get.toNamed(RouteName.listNews);
            },
          ),
          DrawerListTile(
            iconData: Icons.logout,
            title: "Logout",
            onTilePressed: (){
              _signOut().then((value) =>Navigator.of(context)
                  .pushReplacement(MaterialPageRoute(
                    builder: (context) => Login())));
            },
          )
        ],
      ),
    );
  }
}

class DrawerListTile extends StatelessWidget {
  final IconData? iconData;
  final String? title;
  final VoidCallback? onTilePressed;

  const DrawerListTile({Key? key, this.iconData, this.title, this.onTilePressed})
  :super(key: key);
  @override
  Widget build(BuildContext contex){
    return ListTile(
      onTap: onTilePressed,
      dense: true,
      leading: Icon(iconData),
      title: Text(title!, style: TextStyle(fontSize: 16),),
    );
  }
}