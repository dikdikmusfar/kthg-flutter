import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:kthg/pages/LoginScreen.dart';
import 'package:kthg/pages/Menu/DrawerScreen.dart';
import 'package:kthg/routes/route_name.dart';
import 'package:get/get.dart';

class HomeScreen extends StatelessWidget {

  Future<void> _signOut() async {
    await FirebaseAuth.instance.signOut();
  }

  @override
  Widget build(BuildContext context) {
    FirebaseAuth auth = FirebaseAuth.instance;
    if(auth.currentUser != null){
      print(auth.currentUser!.email);
    }
    return Scaffold(
      appBar: AppBar(
        title: Text("Home"),
        actions: <Widget>[
          Padding(padding: const EdgeInsets.all(8.0),
          
          )
        ],
      ),
      drawer: DrawerScreen(),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 17),
            Text.rich(
              TextSpan(
                children: <TextSpan>[
                  TextSpan(
                    text: "Halo!\n",
                    style: TextStyle(color: Colors.blue[400]),
                  ),
                  TextSpan(
                    text: auth.currentUser!.email,
                    style: TextStyle(color: Colors.red[400]),
                  ),
                ],
              ),
              style: TextStyle(fontSize: 24),
            ),
            Text("______________________________________________________"),
            SizedBox(height: 30),
            Text("Hot News",
                style: TextStyle(fontWeight: FontWeight.w600, fontSize: 20, color: Colors.red[400])),
            SizedBox(height: 10.0),
            Container(
              padding: EdgeInsets.all(10),
              child: Row(children: <Widget>[
                Image.asset("assets/img/girl.png", height: 100, width: 100),
                Text("     "),
                Text("an influencer in Indonesia\nallegedly escaped from\nthe quarantine period,\naccording to one twitter user,\nthe influencer only underwent\nquarantine for 3 days..."),
              ],),
            ),
            SizedBox(height: 10.0),
            Container(
              padding: EdgeInsets.all(10),
              child: Row(children: <Widget>[
                Text("tolak pernikahan dini,\nmenikahlah ketika sudah\nsiap secara mental,\nmateri, dll. Terbukti\nbahwa tingkat perceraian\nsemakin naik di tahun 2021.."),
                Image.asset("assets/img/rejected.png", height: 150, width: 150),
              ],),
            ),
            ElevatedButton(
                // textColor: Colors.white,
                style: newraisedButtonStyle,
                child: Text("Read more news"),
                onPressed: () async {
                  Get.toNamed(RouteName.listNews);
                },
              ),
            Container(
              child: ElevatedButton(
                style: raisedButtonStyle,
                onPressed: () {
                  _signOut().then((value) =>Navigator.of(context)
                  .pushReplacement(MaterialPageRoute(
                    builder: (context) => Login())));
                },
                child: Text('Logout'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

final ButtonStyle raisedButtonStyle = ElevatedButton.styleFrom(
  onPrimary: Colors.yellow,
  primary: Colors.red[400],
  minimumSize: Size(88, 36),
  padding: EdgeInsets.symmetric(horizontal: 16),
  shape: const RoundedRectangleBorder(
    borderRadius: BorderRadius.all(Radius.circular(6)),
  ),
);

final ButtonStyle newraisedButtonStyle = ElevatedButton.styleFrom(
  onPrimary: Colors.white,
  primary: Colors.blueAccent[400],
  minimumSize: Size(88, 36),
  padding: EdgeInsets.symmetric(horizontal: 16),
  shape: const RoundedRectangleBorder(
    borderRadius: BorderRadius.all(Radius.circular(6)),
  ),
);
