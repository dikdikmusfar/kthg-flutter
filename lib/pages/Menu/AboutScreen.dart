import 'package:flutter/material.dart';

class AboutScreen extends StatelessWidget {
  const AboutScreen({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.all(10),
        child: ListView(
          children: <Widget>[
            Container(
              alignment: Alignment.center,
              padding: EdgeInsets.all(10),
              margin: const EdgeInsets.only(top: 40),
              child: Text(
                "You are extremely",
                style: TextStyle(
                  color: Colors.blue[400],
                  fontWeight: FontWeight.w400,
                  fontSize: 36
                ),
                ),
            ),
            Container(
              alignment: Alignment.center,
              padding: EdgeInsets.all(10),
              child: Text(
                "valuable.",
                style: TextStyle(
                  color: Colors.blue[400],
                  fontWeight: FontWeight.w400,
                  fontSize: 36
                ),
                ),
            ),
            SizedBox(height: 10,),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Image.asset(
                "assets/img/me.jpeg",
                height: 132,
                width: 132,
              ),
            ),
            Container(
              alignment: Alignment.center,
              padding: EdgeInsets.all(10),
              margin: const EdgeInsets.only(top: 10),
              child: Text(
                "Dikdik Musfar",
                style: TextStyle(
                  color: Colors.red[400],
                  fontWeight: FontWeight.w800,
                  fontSize: 22
                ),
                ),
            ),
            SizedBox(height: 10),
            Container(
              alignment: Alignment.center,
              padding: EdgeInsets.all(10),
              margin: const EdgeInsets.only(top: 10),
              child: Text(
                "My Activity",
                style: TextStyle(
                  color: Colors.blue[400],
                  fontWeight: FontWeight.w800,
                  fontSize: 22
                ),
                ),
            ),
            SizedBox(height: 10),
            SizedBox(
              height: 300,
              child: GridView.count(
                padding: EdgeInsets.zero,
                crossAxisCount: 3,
                childAspectRatio: 1.491,
                crossAxisSpacing: 10,
                mainAxisSpacing: 10,
                physics: NeverScrollableScrollPhysics(),
                children: [
                  for (var activity in activities)
                    Image.asset('assets/img/$activity.png')
                  ],
                ),
              )
          ],
        )
      ),
    );
  }
}

final activities = ['1','2','3','4','5','6'];