import 'package:flutter/material.dart';
import 'package:kthg/pages/Api/GetData.dart';
import 'package:kthg/pages/Menu/AboutScreen.dart';
import 'package:kthg/pages/Menu/HomeScreen.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({ Key? key }) : super(key: key);

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  int _selectedIndex = 0;
  final _layoutPage = [
    HomeScreen(),
    GetDataScreen(),
    AboutScreen(),
  ];
  void _onTabItem(int index){
    setState(() {
      _selectedIndex = index;
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _layoutPage.elementAt(_selectedIndex),
      bottomNavigationBar: BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text("Home")
            ),
          BottomNavigationBarItem(
            icon: Icon(Icons.document_scanner),
            title: Text("Author")
            ),
          BottomNavigationBarItem(
            icon: Icon(Icons.account_circle_sharp),
            title: Text("Account")
            ),
        ],
        type: BottomNavigationBarType.fixed,
        currentIndex: _selectedIndex,
        onTap: _onTabItem,
      ),
    );
  }
}